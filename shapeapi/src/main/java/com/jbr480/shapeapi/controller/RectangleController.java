package com.jbr480.shapeapi.controller;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.jbr480.shapeapi.model.Rectangle;

@CrossOrigin
@RestController
public class RectangleController {
    @GetMapping("/rectangle-area")
    public double getRectangleArea(
    @RequestParam(required = true) double width,
    @RequestParam(required = true) double height) {
        Rectangle rectangle = new Rectangle(width, height);
        double area = rectangle.getArea();
        return area;
    }
    @GetMapping("/rectangle-perimeter")
    public double getRectanglePerimeter(
    @RequestParam(required = true) double width,
    @RequestParam(required = true) double height) {
        Rectangle rectangle = new Rectangle(width, height);
        double perimeter = rectangle.getPerimeter();
        return perimeter;
    }
}
