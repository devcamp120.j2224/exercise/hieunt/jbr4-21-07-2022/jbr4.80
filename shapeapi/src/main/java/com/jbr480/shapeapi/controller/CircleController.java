package com.jbr480.shapeapi.controller;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.jbr480.shapeapi.model.Circle;

@CrossOrigin
@RestController
public class CircleController {
    @GetMapping("/circle-area")
    public double getCirleArea(
    @RequestParam(required = true) double radius) {
        Circle circle = new Circle(radius);
        double area = circle.getArea();
        return area;
    }
    @GetMapping("/circle-perimeter")
    public double getCirlePerimeter(
    @RequestParam(required = true) double radius) {
        Circle circle = new Circle(radius);
        double perimeter = circle.getPerimeter();
        return perimeter;
    }
}
