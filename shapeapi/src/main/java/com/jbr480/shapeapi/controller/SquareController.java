package com.jbr480.shapeapi.controller;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.jbr480.shapeapi.model.Square;

@CrossOrigin
@RestController
public class SquareController {
    @GetMapping("/square-area")
    public double getSquareArea(
    @RequestParam(required = true) double side) {
        Square square = new Square(side);
        double area = square.getArea();
        return area;
    }
    @GetMapping("/square-perimeter")
    public double getSquarePerimeter(
    @RequestParam (required = true) double side) {
        Square square = new Square(side);
        double perimeter = square.getPerimeter();
        return perimeter;
    }
}
